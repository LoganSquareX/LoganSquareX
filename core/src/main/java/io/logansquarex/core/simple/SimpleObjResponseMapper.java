package io.logansquarex.core.simple;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import io.logansquarex.core.JsonMapper;
import io.logansquarex.core.LoganSquare;
import io.logansquarex.core.LoganSquareX;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;


@SuppressWarnings("unsafe,unchecked")
public final class SimpleObjResponseMapper extends JsonMapper<SimpleObjResponse> {


    @Override
    public SimpleObjResponse parse(JsonParser jsonParser) throws IOException {
        throw new IOException("you should not call SimpleResponse");
    }

    @Override
    public void parseField(SimpleObjResponse instance, String fieldName, JsonParser jsonParser) throws IOException {
        throw new IOException("you should not call SimpleResponse");
    }


    @Override


    public void serialize(SimpleObjResponse object, JsonGenerator jsonGenerator, boolean writeStartAndEnd) throws IOException {
        if (writeStartAndEnd) {
            jsonGenerator.writeStartObject();
        }
        jsonGenerator.writeNumberField("code", 0);

        if (object != null) {
            jsonGenerator.writeFieldName("data");
          //  jsonGenerator.writeObject(object.getData());
//            for (Object element : object) {
                if (object.getData() != null) {
                    JsonMapper mapper = LoganSquareX.mapperFor(object.getData().getClass());
                    mapper.serialize(object.getData(), jsonGenerator, true);
                }
          //  jsonGenerator.wri
//            }
//            jsonGenerator.writeEndArray();
        }
        if (writeStartAndEnd) {
            jsonGenerator.writeEndObject();
        }
    }


    public String serialize(SimpleObjResponse object) throws IOException {
        StringWriter sw = new StringWriter();
        JsonGenerator jsonGenerator = LoganSquare.JSON_FACTORY.createGenerator(sw);
        serialize(object, jsonGenerator, true);
        jsonGenerator.close();
        return sw.toString();
    }

    /**
     * Serialize a list of objects to a JsonGenerator.
     *
     * @param list          The list of objects to serialize.
     * @param jsonGenerator The JsonGenerator to which the list should be serialized
     */
    public void serialize(SimpleObjResponse list, JsonGenerator jsonGenerator) throws IOException {

        if (list != null) {
            serialize(list, jsonGenerator, true);
        } else {
            jsonGenerator.writeNull();
        }
    }


}
